mod error;
use error::Error;
mod config;
pub use config::Config;
mod utils;
use utils::*;
mod syslog_utils;
use syslog_utils::write;

use pnet::datalink::MacAddr;
use std::{
    collections::HashMap,
    net::Ipv6Addr,
    sync::{mpsc, Arc, Mutex},
    thread,
};

/// Main logic of the program (that’s where the magic happens…)
pub fn run(config: Config) {
    // place config variable into a mutex to be accessible by every thread
    let local_config = Arc::new(Mutex::new(config));
    // checks devices available
    let devices = check_devices(&local_config.lock().unwrap().interfaces);
    // maps the MacAddr to the current IPv6
    let mut targets: HashMap<MacAddr, Vec<Ipv6Addr>> = HashMap::new();

    if devices.is_empty() {
        panic!("None of the devices specified exists!");
    }

    let (tx, rx) = mpsc::channel();

    // Launches one thread per device
    for device in devices {
        // clone mpsc transmitter and local_config for each thread
        let tx = tx.clone();
        let local_config = Arc::clone(&local_config);

        // build the thread itself
        let builder = thread::Builder::new().name(device.name.clone());
        builder
            .spawn(move || {
                let device_name = device.name.clone();
                let mut cap = initiate_capture(device)
                    .unwrap_or_else(|e| panic!("Cannot capture on device {}: {}", device_name, e));

                cap.filter("icmp6 and ip6[40] = 136", true)
                    .unwrap_or_else(|e| panic!("Cannot filter on device {}: {}", device_name, e));

                while let Ok(raw_packet) = cap.next() {
                    let na_packet = decap_raw_packet(raw_packet.data);
                    match na_packet {
                        Ok(n) => {
                            tx.send(n).unwrap_or_else(|e| {
                                write(
                                    local_config.lock().unwrap().stdout,
                                    syslog::Severity::LOG_ERR,
                                    &format!("{}", e),
                                )
                                .unwrap()
                            });
                        }
                        Err(e) => {
                            if e == Error::CannotDecapPacket {
                                write(
                                    local_config.lock().unwrap().stdout,
                                    syslog::Severity::LOG_ERR,
                                    &format!("{}", e),
                                )
                                .unwrap()
                            }
                        }
                    };
                }
            })
            .unwrap_or_else(|e| panic!("Cannot spawn thread: {}", e));
    }

    // Drop the initial tx
    drop(tx);

    // Main thread just collects information
    for received in rx {
        let mut changed = false;

        match targets.get_mut(&received.0) {
            None => {
                targets.insert(received.0, vec![received.1]);
                changed = true;
            }
            Some(i) => {
                if !i.contains(&received.1) {
                    i.push(received.1);
                    changed = true;
                }
            }
        }

        if changed {
            let message = format!("new IP {} for {}", received.1, received.0);
            write(
                local_config.lock().unwrap().stdout,
                syslog::Severity::LOG_INFO,
                &message,
            )
            .unwrap();
        }
    }
}
