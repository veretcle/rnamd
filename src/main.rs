use daemonize::Daemonize;

use rnamd::*;

fn main() {
    let config = Config::new();

    if !config.stdout {
        let daemonize = Daemonize::new().pid_file(&config.pid);
        daemonize.start().expect("Not able to start as a daemon!");
    }

    run(config);
}
