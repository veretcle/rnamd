use clap::{Arg, ArgAction, Command};

/// "Struct config contains the configuration of the program
pub struct Config {
    pub interfaces: Vec<String>,
    pub stdout: bool,
    pub pid: String,
}

impl Config {
    /// Parses the options with clap
    pub fn new() -> Config {
        let matches = Command::new(env!("CARGO_PKG_NAME"))
                            .version(env!("CARGO_PKG_VERSION"))
                            .about("Daemon that listens on ICMPv6 broadcasts on the network and log the IPv6 address changes to syslog")
                            .arg(Arg::new("interface")
                                 .short('i')
                                 .long("interface")
                                 .value_name("IF")
                                 .help("select interface(s) to watch")
                                 .num_args(1)
                                 .required(true)
                                 .action(ArgAction::Append))
                            .arg(Arg::new("stdout")
                                 .short('s')
                                 .long("stdout")
                                 .help("Outputs to stdout instead of syslog")
                                 .action(ArgAction::SetTrue))
                            .arg(Arg::new("pid")
                                 .short('p')
                                 .long("pid")
                                 .value_name("FILE")
                                 .help("PID file for daemon mode")
                                 .default_value("/run/rnamd.pid")
                                 .num_args(1))
                            .get_matches();

        Config {
            interfaces: matches
                .get_many::<String>("interface")
                .unwrap()
                .map(|x| x.to_string())
                .collect(),
            stdout: matches.get_flag("stdout"),
            pid: matches
                .get_one::<String>("pid")
                .expect("PID file cannot be empty!")
                .to_string(),
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self::new()
    }
}
