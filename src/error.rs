use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result},
};

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    /// Address is link-local (we only care about Global Addresses)
    Fe80,
    /// Neighbor Advert Option is empty (option field in NA Packet doesn’t contain any option)
    EmptyNeighborAdvertOption,
    /// Ndp Option Type is not TargetLLAddr (NA Packet is not advertised properly)
    UnrecognizedNdpOptionType,
    /// Packet cannot be decapsulated (something went wrong when trying to decap the
    /// Ether/IPv6/ICMPv6/NA Packet)
    CannotDecapPacket,
}

impl Error {
    fn __description(&self) -> &str {
        match self {
            Error::Fe80 => "Address is link-local",
            Error::EmptyNeighborAdvertOption => "Neighbor Advert Option is empty",
            Error::UnrecognizedNdpOptionType => "Ndp Option Type is not TargetLLAddr",
            Error::CannotDecapPacket => "Packet cannot be decapsulated",
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> Result {
        self.__description().fmt(f)
    }
}

impl StdError for Error {}
