use std::process;
use syslog::{Facility, Formatter3164};

/// Writes to stdout or syslog regarding configuration
pub fn write(stdout: bool, level: syslog::Severity, message: &str) -> Result<(), syslog::Error> {
    // if output is stdout
    if stdout {
        println!("{}", message);
    } else {
        // else: to syslog
        let formatter = Formatter3164 {
            facility: Facility::LOG_USER,
            hostname: None,
            process: "rnamd".into(),
            pid: process::id(),
        };

        let mut logger = syslog::unix(formatter)?;

        match level {
            syslog::Severity::LOG_INFO => logger.info(message)?,
            _ => logger.err(message)?,
        }
    }

    Ok(())
}
