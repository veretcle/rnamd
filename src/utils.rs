use crate::error::Error;

use pcap::{Active, Capture, Device};
use pnet::datalink::MacAddr;
use pnet::packet::*;
use std::net::Ipv6Addr;

/// Checks if devices passed in options are available devices
/// Panics in case one of the device is wrong
/// Note: &Vec<String> is not the most elegant way to pass a String Vector but it’s the simpliest
/// way in that case
pub fn check_devices(devices: &[String]) -> Vec<Device> {
    let mut available_devices = match Device::list() {
        Ok(d) => d,
        Err(e) => panic!("Error while getting device list: {}", e),
    };

    available_devices.retain(|x| devices.contains(&x.name));

    available_devices
}

/// Initiate the capture device
pub fn initiate_capture(device: Device) -> Result<Capture<Active>, pcap::Error> {
    Capture::from_device(device)?
        .promisc(true)
        .immediate_mode(true)
        .snaplen(5000)
        .open()
}

/// Decapsulates the ether packet into an ICMPv6 packet and returns the IPv6 Target Addr and
/// associated MacAddr
pub fn decap_raw_packet(raw_ether_packet: &[u8]) -> Result<(MacAddr, Ipv6Addr), Error> {
    let ether_packet =
        ethernet::EthernetPacket::new(raw_ether_packet).ok_or(Error::CannotDecapPacket)?;
    let ipv6_packet =
        ipv6::Ipv6Packet::new(ether_packet.payload()).ok_or(Error::CannotDecapPacket)?;
    let neighboradvert_packet = icmpv6::ndp::NeighborAdvertPacket::new(ipv6_packet.payload())
        .ok_or(Error::CannotDecapPacket)?;

    let my_options = neighboradvert_packet.get_options();

    if neighboradvert_packet.get_target_addr().segments()[0] == 0xfe80 {
        return Err(Error::Fe80);
    }

    if my_options.is_empty() {
        return Err(Error::EmptyNeighborAdvertOption);
    }

    if my_options[0].option_type != icmpv6::ndp::NdpOptionTypes::TargetLLAddr {
        return Err(Error::UnrecognizedNdpOptionType);
    }

    let dst_mac = MacAddr::new(
        my_options[0].data[0],
        my_options[0].data[1],
        my_options[0].data[2],
        my_options[0].data[3],
        my_options[0].data[4],
        my_options[0].data[5],
    );

    Ok((dst_mac, neighboradvert_packet.get_target_addr()))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_devices_fail() {
        let devices = vec!["ethfake0".to_string()];

        let available_devices = check_devices(&devices);

        assert!(available_devices.is_empty());
    }

    #[test]
    fn test_check_devices_pass() {
        let devices = vec!["lo".to_string()];

        let available_devices = check_devices(&devices);

        assert_eq!(&available_devices[0].name, "lo");
    }
}
