# Rust Neighbor Advertisement Monitoring Daemon

This is a small daemon that listens on ICMPv6 broadcasts on the network and log the IPv6 address changes to syslog.

# Goals

Contrary to DHCP, Router Advertisement doesn’t maintain a leases’ database that would allow you to maintain a correspondance between MAC Address and IPv6 address. This daemon can detect changes on the network and log these correspondance for you.

# Structure

Requires `pcap` and root access to listen to one or more interface. Monitors the field ICMPv6 type 136 (Neighbor Advertisement) and logs the subsequent MAC Address and IPv6 Address to syslog or stdout.

# Usage

```
USAGE:
    rnamd [FLAGS] [OPTIONS] --interface <IF>...

FLAGS:
    -h, --help       Prints help information
    -s, --stdout     Outputs to stdout instead of syslog
    -V, --version    Prints version information

OPTIONS:
    -i, --interface <IF>...    select interface(s) to watch
    -p, --pid <FILE>           PID file for daemon mode
```
